%
% RCHsingle_MDM   Finds the optimal solution to the Reduced Convex Hulls (ERCH) problem for a single class with an adapted clipped-MDM method.
%
%   [w,b,L,U] = RCHsingle_MDM(X,z,eta,lamX,FX,verbose)
%
% The RCH problem  for 1 class takes the form:
%
%   min(w)  ||x-z||_2^2
%   s.t.    x \in Xr
%
% where Xr is the set of points inside a Reduced Convex Hull (RCH) and z is a fixed point
% The RCH for a set of points X is defined as the set:
%
%      RCH(X) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  0 <= a_i <= eta for all i}
%
% for eta the reduction parameter of the RCH.
%
% The solution of the RCH problem for class is equivalent to the classification hyperplane of the nu-SVM when X is the RCH of
% patterns from the positive class, and the negative class is a unique point. Eta relates to nu as: eta = 2 / (nu · N), N number of patterns.
% A meaningful solution can be obtained only when z is not contained in X hull.
%
% Additionally the set of points FX can be provided, referring to points in the RCH X which must
% have a fixed weight nu. If such set is non-empty the clipped-MDM method is slightly adapted to consider these additional
% constraints, which only affect the initialization of w and the calculation of nearest points at the end of the algorithm.
%
% Inputs:
%   - X: Xn x d matrix with set of Xn points of the hull X.
%   - z: reference fixed point.
%   - eta: hull reduction parameter in the range [0,1].
%   - [lamX]: initial dual representation for the extreme point in hull X (default: the algorithm chooses the initialization)
%   - [FX]: points of the hull X with fixed weight eta in the combination. (default: no fixed points)
%   - [verbose]: if true, messages on the evolution of the algorithm are printed throughout the optimization.
%
% Outputs:
%   - Xex: nearest points to z in reduced convex hull X.
%   - lamX: dual representation of nearest point.
%   - w: normal vector of maximum margin hyperplane separating the hull X and the fixed point z.
%   - b: bias of w.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference papers:
%   - Clipping Algorithms for Solving the Nearest Point Problem over Reduced Convex Hulls. Jorge López, Álvaro Barbero, José R. Dorronsoro.
%       Pattern Recognition, vol. 44, no. 3, pp. 607–614, 2011.
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function [Xex,lamX,w,b] = RCHsingle_MDM(X,z,eta,lamX,FX,verbose)
    %%% CONFIG
    
    % Stopping tolerance
    STOP = 1e-3;
    
    %%% RUN
    
    % Check fixed points
    if nargin < 5, FX = []; end;

    % Check feasibility of eta
    Xn = size(X,1);
    minEta = 1. / (Xn+size(FX,1));
    if eta < minEta
        if verbose, fprintf(1,'eta=%f unfeasible (minimum eta=%f). Setting eta to minimum\n',eta,minEta); end;
        eta = minEta;
    end
    nEta = floor(1/eta);
    limit = ceil(1/eta); wlimit = 1-nEta*eta;

    % Initialize dual representation of x
    if nargin < 4
        lamX = zeros(Xn,1); lamX(limit) = wlimit; lamX(1:nEta) = eta;
    else
        if size(lamX,2) > 1, lamX = lamX'; end;
    end
    
    % Default non-verbose
    if ~exist('verbose','var'), verbose = 0; end
    
    % Compute initial hyperplane
    w = sum(X .* repmat(lamX,1,size(X,2)))' - z;
    if length(FX) > 0, w = w + eta * sum(FX,1)'; end;
    
    % Iterate until convergence
    delta = inf;
    iters = 0;
    while delta > STOP
        if verbose, fprintf(1,'--- RCH1_MDM start of iteration %d ---\n',iters+1); end
        
        % Compute margins for S point
        mX = X*w;
    
        %% Find L, U
        % LX: lowest margin with weight < eta
        idx = find(lamX < eta); [dummy i] = min(mX(idx)); LX = idx(i);
        % UX: largest margin with weight > 0
        idx = find(lamX > 0); [dummy i] = max(mX(idx)); UX = idx(i);
        
        % Traces        
        if verbose
            fprintf(1,'w = [ '); fprintf(1,'%e ',w); fprintf(']\n');
            fprintf(1,'mX = [ '); fprintf(1,'%e ',mX); fprintf(']\n');
            fprintf(1,'lamX = [ '); fprintf(1,'%e ',lamX); fprintf(']\n');
            fprintf(1,'LX=%d, UX=%d\n',LX,UX);
        end
        
        % Compute delta
        if length(UX) >= 1 & length(LX) >= 1, delta = mX(UX) - mX(LX); % -y_L included
        else delta = -inf; end;
        % Special case where delta produces no improvement: we are already at the optimum
        if delta == -inf, break; end;
        % Compute update margins
        diff = X(LX,:)' - X(UX,:)'; bottom = lamX(UX); top = eta - lamX(LX);
 
        if verbose
            fprintf(1,'delta=%e, bottom=%e, top=%e\n',delta,bottom,top);
        end
        
        % Compute updating stepsize
        if delta < 0, step = 0;
        else step = delta / (diff'*diff);
        end
        
        % Clip stepsize
        step = min([step, bottom, top]);
        
        % Update primal and dual representations (LIBLINEAR style)
        lamX(UX) = lamX(UX) - step;
        lamX(LX) = lamX(LX) + step;
        w = w + step * diff;
        
        iters = iters+1;
    end
    
    % Get extreme point Xex explicitly (nearest points in the hull to z)
    Xex = sum(X .* repmat(lamX,1,size(X,2)))';
    if length(FX) > 0, Xex = Xex + sum(FX*eta,1)'; end;
    
    % Compute classification bias so that f(1/2 (L + U)) = 0, i.e. the hyperplane w is the bisection of the line joining the extreme point Xex and z
    b = - 0.5 * w' * (Xex+z);
end
