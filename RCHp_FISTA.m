%
% RCHp_FISTA   Finds the optimal solution to the Reduced Convex Hulls (ERCH) problem with lp norm using the proximal FISTA method.
%
%   [w,b,Xex,Zex,lamX,lamZ] = RCHp_FISTA(X,Z,eta,p,lamX,lamZ,FX,FZ,verbose)
%
% The RCH problem for lp norm takes the form:
%
%   min(w)  ||x-z||_p
%   s.t.    x \in Xr, z in Zr
%
% where p >= 1, Xr is the set of points inside a Reduced Convex Hull (RCH) and Zr the set of points of
% a second RCH. The RCH for a set of points X is defined as the set:
%
%      RCH(X) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  0 <= a_i <= eta for all i}
%
% for eta the reduction parameter of the RCH.
%
% The solution of the RCH problem is equivalent to the classification hyperplane of the lq norm nu-SVM when X and Z are the RCH of
% patterns from the positive and negative classes, respectively. Eta relates to nu as: eta = 2 / (nu · N), N number of patterns,
% and the norm q relates to p as 1/p + 1/q = 1. A meaningful solution can be obtained only when the two reduced hulls do not intersect.
%
% Additionally the sets of points FX and FZ can be provided, referring to points in the RCH X and Z (respectively) which must
% have a fixed weight nu. If these sets are non-empty the clipped-MDM method is slightly adapted to consider these additional
% constraints, which only affect the initialization of w and the calculation of nearest points at the end of the algorithm.
%
% Inputs:
%   - X: Xn x d matrix with set of Xn points of the hull X.
%   - Z: Zn x d matrix with set of Zn points of the hull Z.
%   - eta: hull reduction parameter in the range [0,1].
%   - p: value of the lp norm to use, in the range (1,inf).
%   - [lamX,lamZ]: initial dual representations for extreme points in hulls X and Z (default: the algorithm chooses the initialization)
%   - [FX,FZ]: points of the hull X and Z with fixed weight eta in the combination. (default: no fixed points)
%   - [verbose]: if true, messages on the evolution of the algorithm are printed throughout the optimization.
%
% Outputs:
%   - w: normal vector of maximum margin hyperplane separating the hulls.
%   - b: bias of w.
%   - Xex,Zex: nearest points in reduced convex hulls.
%   - lamX,lamZ: dual representations of nearest points.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference papers:
%   - lp norm RCH: Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%   - FISTA algorithm: A Fast Iterative Shrinkage-Thresholding Algorithm for Linear Inverse Problems. Amir Beck and Marc Teboulle. SIAM J. IMAGING SCIENCES, Vol. 2, No. 1, pp. 183–202.
%
function [w,b,Xex,Zex,lamX,lamZ] = RCHp_FISTA(X,Z,eta,p,lamX,lamZ,FX,FZ,verbose)
    %%% CONFIG
    
    % Stopping tolerance
    STOP = 1e-5;
    % Initial Lipschitz estimate
    LINI = 2;
    % Lipschitz estimate scaling factor
    LSCALE = 2;
    % Tolerance for detecting the trivial w = 0 case
    TRIVIAL_TOL = 1e-3;
    % Maximum number of iterations
    MAX_ITERS = 1000;
    % Limit norm for approximating with p = 1
    LOW_P = 1.001;
    % Limit norm for approximating with p = inf
    HIGH_P = 100;
    
    %%% RUN
    
    % Check fixed points
    if nargin < 7, FX = []; end;
    if nargin < 8, FZ = []; end;
    % Default non-verbose
    if ~exist('verbose','var'), verbose = 0; end;
    
    % Special case p = 2: just call the standard MDM algorithm
    if p == 2
        [w,b,Xex,Zex,lamX,lamZ] = RCH_MDM(X,Z,eta,lamX,lamZ,FX,FX);
        return;
    end
    
    % If p ~= 1, and no FX/FZ provided, approximate with p = 1 solver
    if p < LOW_P & length(FX) == 0 & length(FZ) == 0
        [w,b,Xex,Zex,lamX,lamZ] = RCH1_LP(X,Z,eta,p);
        return;
    end
    % If p == 1, approximate with differentiable 1.001
    if p == 1, p = 1.001; end
    % If p >> 1, and no FX/FZ provided, approximate with p = inf solver
    if p > HIGH_P & length(FX) == 0 & length(FZ) == 0
        [w,b,Xex,Zex,lamX,lamZ] = RCHinf_LP(X,Z,eta,p);
        return;
    end
    % If p == inf, approximate with 100
    if p == inf, p = 100; end;

    % Check feasibility of eta
    Xn = size(X,1); Zn = size(Z,1);
    minEta = 1. / min([Xn+size(FX,1), Zn+size(FZ,1)]);
    if eta < minEta
        if verbose, fprintf(1,'eta=%f unfeasible (minimum eta=%f). Setting eta to minimum\n',eta,minEta); end;
        eta = minEta;
    end
    nEta = floor(1/eta);
    limit = ceil(1/eta); wlimit = 1-nEta*eta;

    % Initialize dual representation of x and z
    if nargin < 5
        lamX = zeros(Xn,1); lamX(limit) = wlimit; lamX(1:nEta) = eta;
    else
        if size(lamX,2) > 1, lamX = lamX'; end;
    end
    if nargin < 6
        lamZ = zeros(Zn,1); lamZ(limit) = wlimit; lamZ(1:nEta) = eta;
    else
        if size(lamZ,2) > 1, lamZ = lamZ'; end;
    end
    
    % Use as starting point the solution for p = 2
    [w,b,Xex,Zex,lamX,lamZ] = RCH_MDM(X,Z,eta,lamX,lamZ,FX,FZ);
    % If w too close to 0, the hulls intersect and we are done
    if norm(w) < TRIVIAL_TOL
        if verbose, fprintf(1,'Trivial solution detected\n'); end
        return
    end
    
    % Initialize FISTA
    L = LINI;
    Vx = sum(X .* repmat(lamX,1,size(X,2)))';
    Vz = sum(Z .* repmat(lamZ,1,size(Z,2)))';
    Yx = Vx; Yz = Vz;
    t = 1;
    delta = inf;
    iters = 0;
    while delta > STOP & iters < MAX_ITERS
        if verbose, fprintf(1,'--- RCHp_FISTA start of iteration %d ---\n',iters+1); end
        % Store current parameters
        VxPrev = Vx;    VzPrev = Vz;
        % Compute gradient of l_p norm of w
        df = Yx - Yz;
        nrm = norm(df , p);
        Gx = (abs(df) / nrm).^(p-1) .* sign(df);
        Gz = -Gx;
        
        % Backtracking procedure
        i = 0; search = 1;
        Lprev = L;
        while search == 1
            % Compute L
            L = LSCALE^i * Lprev;
            if verbose, fprintf(1,'L = %g\n',L); end
            
            % Compute reference points for proximity
            Rx = Yx - 1./L * Gx;  Rz = Yz - 1./L * Gz;             
            if verbose
                fprintf(1,'Rx = [ '); fprintf(1,'%e ',Rx); fprintf(']\n');
                fprintf(1,'Rz = [ '); fprintf(1,'%e ',Rz); fprintf(']\n');
            end
            
            % Solve proximity through two simplified ERCH problems
            [Vx,lamX] = RCHsingle_MDM(X,Rx,eta,lamX,FX);
            [Vz,lamZ] = RCHsingle_MDM(Z,Rz,eta,lamZ,FZ);
            if verbose
                fprintf(1,'Vx = [ '); fprintf(1,'%e ',Vx); fprintf(']\n');
                fprintf(1,'Vz = [ '); fprintf(1,'%e ',Vz); fprintf(']\n');
            end
            
            % Compute obj value at proximity point
            fval = norm(Vx - Vz, p);
            % Compute value of quadratic approximation model
            qval = norm(Yx - Yz, p) + ([Vx;Vz] - [Yx;Yz])' * [Gx;Gz] + L/2 * norm([Vx;Vz] - [Yx;Yz])^2;
            % Stop if fval <= qval
            if fval <= qval
                search = 0;
            % Else, increase i and retry
            else
                i = i + 1;
            end
        end
        
        % Update stepsize
        tPrev = t;
        t = (1 + sqrt(1 + 4 * t^2)) / 2.;
        % Update Y
        ratio = (tPrev - 1) / t;
        Yx = Vx + ratio * (Vx - VxPrev);
        Yz = Vz + ratio * (Vz - VzPrev);
        
        if verbose
            fprintf(1,'Yx = [ '); fprintf(1,'%e ',Yx); fprintf(']\n');
            fprintf(1,'Yz = [ '); fprintf(1,'%e ',Yz); fprintf(']\n');
        end
        
        % Stopping criterion
        delta = norm([VxPrev;VzPrev] - [Vx;Vz],inf);
        iters = iters + 1;
        
        % FISTA PSC
        % xPrev = x;
        % r = y - 1/L grad(y);
        % x = prox_L (y) = min_x r(x) + L/2 * ||x - r||^2
        % tPrev = t;
        % t = (1 + sqrt(1 + 4 * t^2)) / 2
        % y = x + (tPrev - 1) / t * (x - xPrev)
    end
    
    % Check termination
    if iters == MAX_ITERS
        disp 'WARNING: MAX_ITERS reached'
    end
    
    % Compute hyperplane
    w = sum([X .* repmat(lamX,1,size(X,2)); - Z .* repmat(lamZ,1,size(Z,2))])';
    if length(FX) > 0, w = w + eta * sum(FX,1)'; end;
    if length(FZ) > 0, w = w - eta * sum(FZ,1)'; end;
    
    % Get extreme points Xex and Uex explicitly (nearest points from classes)
    Xex = sum(X .* repmat(lamX,1,size(X,2)))';
    if length(FX) > 0, Xex = Xex + sum(FX*eta,1)'; end;
    Zex = sum(Z .* repmat(lamZ,1,size(Z,2)))';
    if length(FZ) > 0, Zex = Zex - sum(FZ*eta,1)'; end;
    
    % Compute classification bias
    b = computeBias(X,Z,w,lamX,lamZ,eta);
end

%%% AUXILIARY FUNCTIONS for testing %%%%%%%%%%%%%%

% Objective function
function val = fObj(x,p)
    l = length(x)/2;
    val = norm(x(1:l) - x(l+1:end) , p);
end

% Gradient function
function g = grad(x,p)
    l = length(x)/2;
    df = x(1:l) - x(l+1:end);
    nrm = norm(df , p);
    g = (abs(df) / nrm).^(p-1) .* sign(df);
    g = [g;-g];
end
