%
% MNS_MDM   Finds the minimum norm subgradient for the Langrangian of the ERCH problem using an adapted clipped-MDM method.
%
%   [g Sx Sz Qx Qz Xmu Zmu] = MNS_MDN(X,Z,eta,n,w,Sx,Sz,Qx,Qz,Xmu,Zmu,g,verbose)
%
% The subgradient of the Lagrangian in ERCH takes the form
%
%   min_g ||g - (g · n) / (n · n) n||^2_2
%
% where g is a subgradient of the subdifferential of f(w) (the ERCH objective function) for the current w
% and n is the precomputed gradient of the constraint ||w||_p of ERCH.
%
% Inputs:
%   - X: Xn x d matrix with set of Xn points of the hull X.
%   - Z: Zn x d matrix with set of Zn points of the hull Z.
%   - eta: hull reduction parameter in the range [0,1].
%   - n: gradient of the ERCH constraint (for the current ERCH w value).
%   - Sx: indexes of variable components of the gradient in the hull X.
%   - Sz: indexes of variable components of the gradient in the hull Z.
%   - Qx: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Qz: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Xmu: convex weights of the patterns from hull X.
%   - Zmu: convex weights of the patterns from hull Z.
%   - [g]: subgradient of f at the current w (any subgradient is OK)
%   - [verbose]: if true, messages on the evolution of the algorithm are printed throughout the optimization.
%
% Outputs:
%   - g: minimum norm subgradient of the Lagrangian of f. Note this is different from the input g.
%   - Sx,Sz,Qx,Qz,Xmu,Zmu: index sets and weights modified properly to represent the subgradient found.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function [g Sx Sz Qx Qz Xmu Zmu] = MNS_MDN(X,Z,eta,n,w,Sx,Sz,Qx,Qz,Xmu,Zmu,g,verbose)
    %%% CONFIG
    
    % Stopping tolerance
    STOP = 1e-5;
    
    %%% RUN
    
    % Check arguments
    if ~exist('Xmu','var') error('Argument Xmu missing'); end
    if ~exist('Zmu','var') error('Argument Zmu missing'); end
    if ~exist('verbose','var') verbose = 0; end;
    
    % Weight for limit point
    lim = 1 - eta * floor(1/eta);
    
    % Easy case: if both Sp and Sn are singletons, the subdifferential of the function collapses to a single gradient
    if length(Sx) == 1 & length(Sz) == 1
        % If not provided, compute the only possible gradient 
        if ~exist('g','var')
            L = eta * sum(X(Qx,:),1) + sum(repmat(Xmu',1,size(X,2)) .* X(Sx,:),1);
            U = eta * sum(Z(Qz,:),1) + sum(repmat(Zmu',1,size(Z,2)) .* Z(Sz,:),1);
            g = U - L;
        end
        % Add the effect of the constraint (supporting projection)
        g = g - (g' * n) / (n' * n) * n;
        return;
    end
    
    %%% Hard case: the MNS-MDM algorithm must be used to obtain the minimum norm subgradient
    
    % Rotate n vector if not in column form
    if size(n,1) < size(n,2), n = n'; end;
    
    % Compute N matrix accounting for the influence of the constraint
    % Multiply also N by -1, as the MDM solver supposes w = Xex - Zex, while for the ERCH it is w = Zex - Xex
    len = length(n);
    N = -(eye(len) - (n * n') / (n' * n));
    
    % Scale by N the patterns in the S sets
    NX = X(Sx,:) * N;   NZ = Z(Sz,:) * N;
    % Scale by N the patterns in the Q sets
    NXQ = X(Qx,:) * N;   NZQ = Z(Qz,:) * N;
    
    % Invoking RCH-MDM with the transformed data solves the Minimum Norm Subgradient problem
    [g,b,Xex,Zex,Xmu,Zmu] = RCH_MDM(NX,NZ,eta,Xmu,Zmu,NXQ,NZQ,verbose);
    
    %% Remove 0 weights from S sets
    
    % X hull
    idx = find(iszero(Xmu));
    if length(idx) > 0 & length(idx) == length(Xmu), idx(end) = []; end;
    Sx(idx) = [];   Xmu(idx) = [];
    
    % Z hull
    idx = find(iszero(Zmu));
    if length(idx) > 0 & length(idx) == length(Zmu), idx(end) = []; end;
    Sz(idx) = [];   Zmu(idx) = [];
    
    %% Move eta weigths from S to Q sets, but keeping always at least one point in S
    
    % X hull
    idx = find(iszero(Xmu-eta));
    % If all S points have eta weight, leave one in S, move the rest to Q
    if length(idx) > 0 & length(idx) == length(Xmu), idx(end) = []; end;
    Qx = [Qx; Sx(idx)];   Sx(idx) = [];   Xmu(idx) = [];
    
    % Z hull
    idx = find(iszero(Zmu-eta));
    if length(idx) > 0 & length(idx) == length(Zmu), idx(end) = []; end;
    Qz = [Qz; Sz(idx)];   Sz(idx) = [];   Zmu(idx) = [];
end
