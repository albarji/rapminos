%
% RAPMINOS_example_reduced   Visualizes an example of the RAPMINOS algorithm for a 2D toy dataset with intersecting classes and
% hull reduction
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%   

% Generate random dataset with intersecting classes
N = 100;
X = randn(N,2);
Z = randn(N,2)+1;

% Show RAPMINOS execution
[w,b] = RAPMINOS( X, ... % Points of the first hull
    Z, ... % Points of the second hull
    0.5, ... % Hull reduction coefficient (half)
    2, ... % Norm to use (l2)
    [], ... % Starting point (let RAPMINOS decide)
    2,  ... % Number of approximating eta steps
    1); % Verbose

