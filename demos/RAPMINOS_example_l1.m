%
% RAPMINOS_example_l1   Visualizes an example of the RAPMINOS algorithm for a 2D toy dataset with intersecting classes and l1 norm
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%   

% Generate random dataset with intersecting classes
N = 200;
X = rand(N,2);
Z = rand(N,2)+0.25;

% Show RAPMINOS execution
[w,b] = RAPMINOS( X, ... % Points of the first hull
    Z, ... % Points of the second hull
    1, ... % Hull reduction coefficient (none)
    1, ... % Norm to use (l1)
    [], ... % Starting point (let RAPMINOS decide)
    2,  ... % Number of approximating eta steps
    1); % Verbose

