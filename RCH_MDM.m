%
% RCH_MDM   Finds the optimal solution to the Reduced Convex Hulls (ERCH) problem using the clipped-MDM method.
%
%   [w,b,Xex,Zex,lamX,lamZ] = RCH_MDM(X,Z,eta,lamX,lamZ,FX,FZ,verbose)
%
% The RCH problem takes the form:
%
%   min(w)  ||x-z||_2^2
%   s.t.    x \in Xr, z in Zr
%
% where Xr is the set of points inside a Reduced Convex Hull (RCH) and Zr the set of points of
% a second RCH. The RCH for a set of points X is defined as the set:
%
%      RCH(X) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  0 <= a_i <= eta for all i}
%
% for eta the reduction parameter of the RCH.
%
% The solution of the RCH problem is equivalent to the classification hyperplane of the nu-SVM when X and Z are the RCH of
% patterns from the positive and negative classes, respectively. Eta relates to nu as: eta = 2 / (nu · N), N number of patterns.
% A meaningful solution can be obtained only when the two reduced hulls do not intersect.
%
% Additionally the sets of points FX and FZ can be provided, referring to points in the RCH X and Z (respectively) which must
% have a fixed weight nu. If these sets are non-empty the clipped-MDM method is slightly adapted to consider these additional
% constraints, which only affect the initialization of w and the calculation of nearest points at the end of the algorithm.
%
% Inputs:
%   - X: Xn x d matrix with set of Xn points of the hull X.
%   - Z: Zn x d matrix with set of Zn points of the hull Z.
%   - eta: hull reduction parameter in the range [0,1].
%   - [lamX,lamZ]: initial dual representations for extreme points in hulls X and Z (default: the algorithm chooses the initialization)
%   - [FX,FZ]: points of the hull X and Z with fixed weight eta in the combination. (default: no fixed points)
%   - [verbose]: if true, messages on the evolution of the algorithm are printed throughout the optimization.
%
% Outputs:
%   - w: normal vector of maximum margin hyperplane separating the hulls.
%   - b: bias of w.
%   - Xex,Zex: nearest points in reduced convex hulls.
%   - lamX,lamZ: dual representations of nearest points.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference papers:
%   - Clipping Algorithms for Solving the Nearest Point Problem over Reduced Convex Hulls. Jorge López, Álvaro Barbero, José R. Dorronsoro.
%       Pattern Recognition, vol. 44, no. 3, pp. 607–614, 2011.
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function [w,b,Xex,Zex,lamX,lamZ] = RCH_MDM(X,Z,eta,lamX,lamZ,FX,FZ,verbose)
    %%% CONFIG
    
    % Stopping tolerance
    STOP = 1e-4;
    
    %%% RUN
    
    % Check fixed points
    if nargin < 6, FX = []; end;
    if nargin < 7, FZ = []; end;

    % Check feasibility of eta
    Xn = size(X,1); Zn = size(Z,1);
    minEta = 1. / min([Xn+size(FX,1), Zn+size(FZ,1)]);
    if eta < minEta
        if verbose, fprintf(1,'eta=%f unfeasible (minimum eta=%f). Setting eta to minimum\n',eta,minEta); end;
        eta = minEta;
    end
    nEta = floor(1/eta);
    limit = ceil(1/eta); wlimit = 1-nEta*eta;

    % Initialize dual representation of x and z
    if ~exist('lamX','var') | length(lamX) == 0
        lamX = zeros(Xn,1); lamX(limit) = wlimit; lamX(1:nEta) = eta;
    else
        if size(lamX,2) > 1, lamX = lamX'; end;
    end
    if ~exist('lamZ','var') | length(lamZ) == 0
        lamZ = zeros(Zn,1); lamZ(limit) = wlimit; lamZ(1:nEta) = eta;
    else
        if size(lamZ,2) > 1, lamZ = lamZ'; end;
    end
    
    % Default non-verbose
    if ~exist('verbose','var'), verbose = 0; end
    
    % Compute initial hyperplane
    w = sum([X .* repmat(lamX,1,size(X,2)); - Z .* repmat(lamZ,1,size(Z,2))])';
    if length(FX) > 0, w = w + eta * sum(FX,1)'; end;
    if length(FZ) > 0, w = w - eta * sum(FZ,1)'; end;
    
    % Iterate until convergence
    delta = inf;
    iters = 0;
    while delta > STOP
        if verbose, fprintf(1,'--- RCH_MDM start of iteration %d ---\n',iters+1); end
        
        % Compute margins for S points
        mX = X*w;   mZ = Z*w;
    
        %% Find L, U in each hull
        % LX: lowest margin with weight < eta
        idx = find(lamX < eta); [dummy i] = min(mX(idx)); LX = idx(i);
        % UX: largest margin with weight > 0
        idx = find(lamX > 0); [dummy i] = max(mX(idx)); UX = idx(i);
        % LZ: largest margin with weight < eta
        idx = find(lamZ < eta); [dummy i] = max(mZ(idx)); LZ = idx(i);
        % UZ: lowest margin with weight > 0
        idx = find(lamZ > 0); [dummy i] = min(mZ(idx)); UZ = idx(i);
        
        % Traces        
        if verbose
            fprintf(1,'w = [ '); fprintf(1,'%e ',w); fprintf(']\n');
            fprintf(1,'mX = [ '); fprintf(1,'%e ',mX); fprintf(']\n');
            fprintf(1,'mZ = [ '); fprintf(1,'%e ',mZ); fprintf(']\n');
            fprintf(1,'lamX = [ '); fprintf(1,'%e ',lamX); fprintf(']\n');
            fprintf(1,'lamZ = [ '); fprintf(1,'%e ',lamZ); fprintf(']\n');
            fprintf(1,'LX=%d, UX=%d, LZ=%d, UZ=%d\n',LX,UX,LZ,UZ);
        end
        
        % Select hull to update
        if length(UX) >= 1 & length(LX) >= 1, deltaX = mX(UX) - mX(LX); % -y_L included
        else deltaX = -inf; end;
        if length(UZ) >= 1 & length(LZ) >= 1, deltaZ = mZ(LZ) - mZ(UZ); % -y_L included
        else deltaZ = -inf; end;
        % Special case where both delta produce no improvement: we are already at the optimum
        if deltaX == -inf & deltaZ == -inf, break; end;
        % Choose delta with best improvement
        if deltaX > deltaZ,
            class = 1; delta = deltaX; diff = X(LX,:)' - X(UX,:)'; bottom = lamX(UX); top = eta - lamX(LX);
        else
            class = -1; delta = deltaZ; diff = Z(LZ,:)' - Z(UZ,:)'; bottom = lamZ(UZ); top = eta - lamZ(LZ);
        end
        
        if verbose
            fprintf(1,'class=%d, delta=%e, bottom=%e, top=%e\n',class,delta,bottom,top);
        end
        
        % Compute updating stepsize
        if delta < 0, step = 0;
        else step = delta / (diff'*diff);
        end
        
        % Clip stepsize
        step = min([step, bottom, top]);
        
        % Update primal and dual representations (LIBLINEAR style)
        % Set coefficient to exactly 0 or eta if needed
        if class == 1
            lamX(UX) = lamX(UX) - step;
            lamX(LX) = lamX(LX) + step;
            w = w + step * diff;
        else
            lamZ(UZ) = lamZ(UZ) - step;
            lamZ(LZ) = lamZ(LZ) + step;
            w = w - step * diff;
        end
        
        iters = iters+1;
    end
    
    % Get extreme points Xex and Uex explicitly (nearest points from classes)
    Xex = sum(X .* repmat(lamX,1,size(X,2)))';
    if length(FX) > 0, Xex = Xex + sum(FX*eta,1)'; end;
    Zex = sum(Z .* repmat(lamZ,1,size(Z,2)))';
    if length(FZ) > 0, Zex = Zex - sum(FZ*eta,1)'; end;
    
    % Compute classification bias
    b = computeBias(X,Z,w,lamX,lamZ,eta);
end
