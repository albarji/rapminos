RAPMINOS
========

Matlab toolbox for solving the Extended Reduced Convex Hulls Nearest Points Problem 
(ERCH-NPP) through the RAdially Projected MInimum NOrms Subgradient (RAPMINOS) 
solver.

For an up-to-date version, check https://bitbucket.org/albarji/rapminos .

Index
-----

1. Installation
2. Quickstart
3. Usage
4. Examples
5. Copyright
6. Contact

1. Installation
---------------

While in Matlab, move to RAPMINOS folder and type "install".
If for whatever reason this fails, just add the RAPMINOS folder and subfolders to
Matlab's path. You can do this e.g. by using the "pathtool()" utility.

2. Quickstart
-------------

After installation you can start by checking out some RAPMINOS demos. Just type one
of the following on you Matlab prompt

* RAPMINOS_example                - simple example with a 2D toy dataset
* RAPMINOS_example_reduction      - example with hulls reduction
* RAPMINOS_example_l1             - l1 norm example
* RAPMINOS_example_linf           - linfinity norm example

3. Usage
--------

The ERCH-NPP takes the form:

   min(w)  max(Xr,Zr)  w · Zr  -  w · Xr
   s.t.    ||w||_p = 1

where w is a separating hyperplane, Xr the set of points inside a Reduced Convex 
Hull (RCH) and Zr the set of points of
a second RCH. The RCH for a set of points X is defined as the set:

   RCH(X) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  
                 0 <= a_i <= eta for all i}

for eta the reduction parameter of the RCH.

The RAPMINOS solver finds a "reasonably good" local minimum of this non-convex 
problem. The entry point to the solver is the RAPMINOS function, whose most basic 
usage follows the line

    [w,b] = RAPMINOS(X,Z,eta,p)
    
where X and Z are pattern matrices containing the patterns of the Xr and Zr hulls
(or classes), respectively, one pattern per row. The norm degree p must meet p >= 1.
p = Inf is also accepted. If this parameter is not specified the standard SVM value
p=2 is assumed.

The values (w,b) returned constitute the classification hyperplane and bias found.
Classification of new patterns can be done as sign(N*w + b) for N pattern matrix.

Details on more advanced use of this routine are provided at the header of the
RAPMINOS.m file. Note that the method includes a "verbose" mode which prints step by
step traces of the algorithm process. For 2-dimensional input data it also shows 
some nice plots on the algorithm's current solution at each step. Keep in mind that
these verbose features were just designed for debugging and thus might be terribly
slow and/or buggy.


Additionaly a modified implementation of the clipped-MDM algorithm for the standard
RCH-NPP is included as the RCH_MDM function. Please look at the header of the
RCH_MDM.m file if you are interested in using it.


4. Examples
-----------

First of all, generate a toy dataset with

    N = 100;
    X = randn(N,2);
    Z = randn(N,2)+1;
    
The convex hulls of these X and Z sets will largely intersect, producing a problem
unsolvable by standard RCH-NPP methods unless a small reduction value eta is used.
The following examples show how a sensible solution is found in the ERCH-NPP model:

- Find a solution using the default norm (lp=2) and the full hulls (eta = 1)
    [w,b] = RAPMINOS(X,Z,1,2,[],2,1);
    
- lp=1 and lp=inf norms are also solved:
    [w,b] = RAPMINOS(X,Z,1,1,[],2,1);
    [w,b] = RAPMINOS(X,Z,1,inf,[],2,1);
    
- Smaller reduction parameters are possible:
    [w,b] = RAPMINOS(X,Z,0.5,2,[],2,1);
    
- If the reduction parameter is very small, the problem can be simplified to an
standard RCH-NPP, but RAPMINOS solves it anyway:
    [w,b] = RAPMINOS(X,Z,0.02,2,[],2,1);
    
5. Copyright
------------

This toolbox was developed for the paper

    Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda,
        Jorge López. Publication pending.

All rights reserved. Please do reference the paper if you have used this toolbox in
your research. For commercial uses, please contact the authors.

6. Contact
----------

For any questions and comments, please email alvaro.barbero@uam.es

