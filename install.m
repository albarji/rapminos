%
% install.m
%
% Installation script for RAPMINOS.
%
% Just type "install" while standing at RAPMINOS folder.
%

here = pwd();
addpath(here);
addpath([here,'/demos']);
savepath();

disp('RAPMINOS successfully installed.');

