%
% intersectingEta.m
%
% Finds the eta value for which Reduced Convex Hulls start intersecting.
% This is done by solving the following linear optimization problem:
%
%   min(lambda,eta)  eta
%   s.t.  Xr = Zr
%         Xr in RCH(X,eta)
%         Zr in RCH(Z,eta)
%         
% and the definition of Reduced Convex Hull is given by
%
%   RCH(X,eta) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  0 <= a_i <= eta for all i}
%
% Inputs:
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%
% Outputs:
%   - eta: eta value for which the hulls intersect at exactly 1 point. For values of eta larger
%     or equal than this, the intersection of hulls is non-empty.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function eta = intersectingEta(X,Z)
    % Sizes of data
    Xn = size(X,1);
    Zn = size(Z,1);
    N = Xn + Zn;
    d = size(X,2);

    % Prepare linear optimization problem
    f = [1;
         zeros(Xn,1);
         zeros(Zn,1) ];
    
    Aeq = [ zeros(d,1) , X' , -Z'
            0          , ones(1,Xn) , zeros(1,Zn)
            0          , zeros(1,Xn) , ones(1,Zn)  ];
    beq = [ zeros(d,1)
            1
            1 ];
            
    A = [ -ones(N,1)  ,  eye(N)
          zeros(N,1)  ,  -eye(N) ];
    b = [ zeros(N,1)
          zeros(N,1) ];
          
    % Box constraints are not strictly necessary, but help accelerating the solver
    minEta = 1. / min([Xn Zn]);
    lb = [ minEta
           zeros(Xn,1)
           zeros(Zn,1) ];
    ub = [ 1
           ones(Xn,1)
           ones(Zn,1) ];
           
    % Optimizer options: don't display anything
    opts = optimset('Display','off');
          
    % Invoke linear solver
    sol = linprog(f,A,b,Aeq,beq,lb,ub,[],opts);
    % Extract intersecting eta value
    eta = sol(1);
end
