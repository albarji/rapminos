%
% iszero
%
% Checks within a tolerance whether a quantity is zero.
%
% Inputs:
%   - x: value to check.
%
% Outputs:
%   - res: true if x is zero within the defined tolerance, else false.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function res = iszero(x)
    % Tolerance parameter
    EPSILON = 1e-6;
    % Zero check
    res = (abs(x) < EPSILON);
end
