%
% RCH1_LP   Finds the optimal solution to the linf norm Reduced Convex Hulls (ERCH) problem through a Linear Program
%
%   [w,b,Xex,Zex,lamX,lamZ] = RCHinf_LP(X,Z,eta,verbose)
%
% The RCH problem takes the form:
%
%   min(w)  ||x-z||_inf
%   s.t.    x \in Xr, z in Zr
%
% where Xr is the set of points inside a Reduced Convex Hull (RCH) and Zr the set of points of
% a second RCH. The RCH for a set of points X is defined as the set:
%
%      RCH(X) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  0 <= a_i <= eta for all i}
%
% for eta the reduction parameter of the RCH.
%
% The solution of the RCH problem is equivalent to the classification hyperplane of the l1 norm nu-SVM when X and Z are the RCH of
% patterns from the positive and negative classes, respectively. Eta relates to nu as: eta = 2 / (nu · N), N number of patterns.
% A meaningful solution can be obtained only when the two reduced hulls do not intersect.
%
% Inputs:
%   - X: Xn x d matrix with set of Xn points of the hull X.
%   - Z: Zn x d matrix with set of Zn points of the hull Z.
%   - eta: hull reduction parameter in the range [0,1].
%   - [verbose]: if true, messages on the evolution of the algorithm are printed throughout the optimization.
%
% Outputs:
%   - w: normal vector of maximum margin hyperplane separating the hulls.
%   - b: bias of w.
%   - Xex,Zex: nearest points in reduced convex hulls.
%   - lamX,lamZ: dual representations of nearest points.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference papers:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function [w,b,Xex,Zex,lamX,lamZ] = RCHinf_LP(X,Z,eta,verbose)
    % Check feasibility of eta
    Xn = size(X,1); Zn = size(Z,1);
    minEta = 1. / min([Xn, Zn]);
    if eta < minEta
        if verbose, fprintf(1,'eta=%f unfeasible (minimum eta=%f). Setting eta to minimum\n',eta,minEta); end;
        eta = minEta;
    end
    
    % Default non-verbose
    if ~exist('verbose','var'), verbose = 0; end
    
    % Extreme case eta = minEta : solution is found in closed form
    if eta == minEta
        % Distribuite weights equally among lambdas
        lamX = 1./size(X,1) * ones(size(X,1),1);
        lamZ = 1./size(Z,1) * ones(size(Z,1),1);
        % Get extreme points Xex and Uex explicitly (nearest points from classes)
        Xex = sum(X .* repmat(lamX,1,size(X,2)))';
        Zex = sum(Z .* repmat(lamZ,1,size(Z,2)))';
        % Compute hyperplane
        w = Xex - Zex;
        % Compute classification bias
        b = computeBias(X,Z,w,lamX,lamZ,eta);        
        % Finished
        return
    end
    
    % Prepare problem in LP standard form
    N = Xn+Zn;
    d = size(X,2);
    f = [1; zeros(N,1)];
    lb = [-inf ; zeros(N,1)];
    ub = [inf ; eta .* ones(N,1)];
    Aeq = [ 0, [ones(1,Xn), zeros(1,Zn)] 
            0, [zeros(1,Xn), ones(1,Zn)] ];
    beq = [1 ; 1];
    XZ = [X; -Z];
    A = [ - ones(d,1) , + XZ' 
          - ones(d,1) , - XZ' ];
    b = [ zeros(d,1); zeros(d,1) ];
    opt = optimset('Display','off');
    
    % Invoke LP solver
    sol = linprog(f,A,b,Aeq,beq,lb,ub,[],opt);
    % Recover lambda values
    lam = sol(2:end);
    % Correct lambda values over eta
    lam(find(lam > eta)) = eta;
    % Split by class
    lamX = lam(1:Xn);    
    lamZ = lam(Xn+1:end);
    
    % Get extreme points Xex and Uex explicitly (nearest points from classes)
    Xex = sum(X .* repmat(lamX,1,size(X,2)))';
    Zex = sum(Z .* repmat(lamZ,1,size(Z,2)))';
    
    % Compute hyperplane
    w = Xex - Zex;
    
    % Compute classification bias
    b = computeBias(X,Z,w,lamX,lamZ,eta);
end
