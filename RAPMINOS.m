%
% RAPMINOS   Finds a local solution to the Extended Reduced Convex Hulls (ERCH) problem using the RAPMINOS method.
%
%   [w,b,L,U,intrsct] = RAPMINOS(X,Z,eta,p,w0,etaSteps,verbose)
%
% The ERCH problem takes the form:
%
%   min(w)  max(Xr,Zr)  w · Zr  -  w · Xr
%   s.t.    ||w||_p = 1
%
% where w is a separating hyperplane, Xr the set of points inside a Reduced Convex Hull (RCH) and Zr the set of points of
% a second RCH. The RCH for a set of points X is defined as the set:
%
%      RCH(X) = {x | x = sum_{i \in X} a_i x_i, sum_{i \in X} a_i = 1,  0 <= a_i <= eta for all i}
%
% for eta the reduction parameter of the RCH.
%
% The solution of the ERCH problem is equivalent to the classification hyperplane of the Enu-SVM when X and Z are the RCH of
% patterns from the positive and negative classes, respectively. Eta relates to nu as: eta = 2 / (nu · N), N number of patterns.
%
% Inputs:
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%   - eta: reduction value for the Reduced Convex Hulls.
%   - [p]: value of the Lp norm of the constraint. Must be in the range [1,inf]. Default is 2.
%   - [w0]: starting point for w.
%   - [etaSteps]: number of approximating eta steps to the solution (default 2)
%   - [verbose]: if true, messages on the evolution of the algorithm are printed throughout the optimization.
%
% Outputs:
%   - w: resultant separating hyperplane.
%   - b: resultant bias of the w hyperplane.
%   - L: nearest point in the X reduced hull.
%   - U: nearest point in the Z reduced hull.
%   - intrsct: flag indicating whether the classes intersect for the given eta value.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%   
function [w,b,L,U,intrsct] = RAPMINOS(X,Z,eta,p,w0,etaSteps,verbose)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% CONFIG %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Stopping tolerance
    STOP = 1e-3;
    % Maximum number of iterations
    MAX_ITERS = 1e6;
    
    % Default number of eta steps
    ETA_STEPS = 2;
    
    % Range for norm contour plot (when in verbose and 2D mode)
    C_RANGE = -1.1:0.025:1.1;
    
    % Separability tolerance for invoking the intersecting case algorithm
    SEP_TOL = 1e-3;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% INPUTS AND INITIALIZATION %%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Analyze inputs
    [Xn dim] = size(X);
    [Zn dim2] = size(Z);
    if dim ~= dim2
        disp 'Error in ERCH_DPS: dimension of datasets X and Z differ'
        return
    end
    
    % Default norm p=2
    if ~exist('p','var'), p = 2;
    % Check value if provided
    else
        if p < 1
            disp 'Error in ERCH_DPS: p norm parameter must lie in the range [1,inf]'
            return
        end
    end
    % Take starting point if available
    if exist('w0','var') & length(w0) == dim, w = w0;
    % Else initialize w randomly so that ||w||_p = 1
    else w = rand(dim,1); end;
    w = w ./ norm(w,p);
    % Defaulte eta steps
    if ~exist('etaSteps','var'), etaSteps = ETA_STEPS; end;
    % Default non-verbose
    if ~exist('verbose','var'), verbose = 0; end;
    
    % Check eta value
    minEta = 1. / min([Xn Zn]);
    if eta < minEta
        disp 'eta value too small: adjusting to minimum value'
        eta = minEta;
    end
    
    % If verbose and 2D, precompute some quantities for illustration purposes
    if verbose & dim == 2
        % Compute explicitly convex hulls
        XCH = convhull(X(:,1),X(:,2));
        ZCH = convhull(Z(:,1),Z(:,2));
        % Plot limits
        plotLimits = [min([X(:,1);Z(:,1)])  max([X(:,1);Z(:,1)])  min([X(:,2);Z(:,2)])   max([X(:,2);Z(:,2)])];
        % Grid of points for norm and function evaluation
        [plotx,ploty] = meshgrid(C_RANGE,C_RANGE);
        for i=1:size(plotx,1)
            for j=1:size(plotx,2)
                % Function value
                [L,U] = extremeMargins(X,Z,[plotx(i,j); ploty(i,j)],eta);
                plotz(i,j) = (U'-L') * [plotx(i,j); ploty(i,j)];
                % Norm value
                plotn(i,j) = norm([plotx(i,j); ploty(i,j)],p);
            end
        end
        bg = plotz;
        bg = (bg-min([plotz(:);-eps])) * 256 / (max([plotz(:);eps]) - min([plotz(:);-eps]));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% RAPMINOS STARTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Initialize dual representations
    lamX = [];
    lamZ = [];
    % Initialize intersecting eta value to undefined
    limitEta = NaN;
    
    % Start at a small eta value (ensures separability), and progress towards the requested one
    etaReq = eta; intrsct = 0; nontrivial = 0;
    if etaReq ~= minEta, range = linspace(minEta,etaReq,etaSteps);
    else range = minEta; end;
    for eta=range
        if verbose, fprintf(1,'Solving for eta=%f...\n',eta); end
        % Precompute some more quantities for illustration purposes
        if verbose & dim == 2
            for i=1:size(plotx,1)
                for j=1:size(plotx,2)
                    % Function value
                    [L,U] = extremeMargins(X,Z,[plotx(i,j); ploty(i,j)],eta);
                    plotz2(i,j) = (U'-L') * [plotx(i,j); ploty(i,j)];
                end
            end
            bg2 = plotz2;
            bg2 = (bg2-min([plotz2(:);-eps])) * 256 / (max([plotz2(:);eps]) - min([plotz2(:);-eps]));
        end
    
        %%%
        %%% Easiest case: if the convex hulls are reduced to their barycentres, the solution can be obtained in closed form
        %%%
        if eta == minEta
            if verbose, disp 'Barycentre case detected'; end
            L = mean(X,1)'; U = mean(Z,1)';
            w = L - U; % Note this is the exact opposite to the w for ERCH
            w = w ./ norm(w,p);
            Xmu = 1./size(X,1) * ones(size(X,1),1);
            Zmu = 1./size(Z,1) * ones(size(Z,1),1);
            b = computeBias(X,Z,w,Xmu,Zmu,eta);
            if verbose
                acc = (sum(sign(X*w + b) == 1) + sum(sign(Z*w + b) == -1)) / (Xn + Zn) * 100;
                if dim == 2, drawEvol(); end;
            end
            lamX = ones(size(X,1),1) ./ size(X,1);
            lamZ = ones(size(Z,1),1) ./ size(Z,1);
            continue
        end
        
        %%%
        %%% Easy case: if no intersection detected yet, try to solve the problem with the separable algorithm
        %%%
        if ~intrsct
            nontrivial = 1;
            if verbose, fprintf(1,'Trying separable case...'); end
            % Solve with the p=2 RCH-MDM approximation
            [wNew,bNew,L,U,lamXNew,lamZNew] = RCH_MDM(X,Z,eta,lamX,lamZ);
            
            % If w too small, we might be in the intersecting case
            if norm(wNew) < SEP_TOL
                if verbose, fprintf(1,' possible intersection: running exact test...'); end
                % In that case, check again with an exact solver
                if isnan(limitEta)
                    limitEta = intersectingEta(X,Z);
                end
                if verbose, fprintf(1,' limit eta = %f,',limitEta); end
                if eta > limitEta
                    if verbose, fprintf(1,' FAIL\n'); end
                    % Normalize w to meet norm constraint
                    w = w ./ norm(w,p);
                    intrsct = 1;
                end
            end
            % If non-intersecting accept the solution for the current eta
            if ~intrsct
                w = wNew; b = bNew; lamX = lamXNew; lamZ = lamZNew; Xmu = lamX; Zmu = lamZ;
                if verbose
                    fprintf(1,' SUCCESS\n');
                    fprintf(1,'w = [ '); fprintf(1,'%e ',w); fprintf(']\n');
                end
                continue
            end
        end
        
        %%%
        %%% Hard case: apply the full RAPMINOS algorithm
        %%%
        nontrivial = 1;
        
        % Obtain points with least / most margin using Mavroforakis method
        [L,U,Sx,Sz,Qx,Qz,Xmu,Zmu] = extremeMargins(X,Z,w,eta);
        
        if verbose
            fprintf(1,'===> Extreme initialization for eta=%e\n',eta);
            fprintf(1,'w = [ '); fprintf(1,'%e ',w); fprintf(']\n');
            fprintf(1,'Qx = [ '); fprintf(1,'%d ',Qx); fprintf(']\n');
            fprintf(1,'Sx = [ '); fprintf(1,'%d ',Sx); fprintf(']\n');
            fprintf(1,'Xmu = [ '); fprintf(1,'%e ',Xmu); fprintf(']\n');
            fprintf(1,'Qz = [ '); fprintf(1,'%d ',Qz); fprintf(']\n');
            fprintf(1,'Sz = [ '); fprintf(1,'%d ',Sz); fprintf(']\n');
            fprintf(1,'Zmu = [ '); fprintf(1,'%e ',Zmu); fprintf(']\n');
        end

        % Iterate until convergence
        stop = inf; fw = inf; iters = 0;    
        while stop > STOP & iters < MAX_ITERS  
            if verbose
                fprintf(1,'=== Start of iteration %d ===\n',iters+1);
            end
            
            % Compute extreme points
            [L,U] = getLU(X,Z,eta,Sx,Sz,Qx,Qz,Xmu,Zmu);
            % Obtain some subgradient of f(w)
            g = U - L;
            
            % Compute objective function value
            prev = fw;
            fw = g' * w;
            
            % End optimization if worsening in objective function.
            % This is impossible in theory, since the updating direction is always a descent direction, but
            % in practice numerical errors can make this happen. If so, it is better to stop the algorithm
            if fw >= prev
                w = wPrev;
                break;
            end
            
            if verbose
                fprintf(1,'w = [ '); fprintf(1,'%e ',w); fprintf(']\n');
                fprintf(1,'Qx = [ '); fprintf(1,'%d ',Qx); fprintf(']\n');
                fprintf(1,'Sx = [ '); fprintf(1,'%d ',Sx); fprintf(']\n');
                fprintf(1,'Xmu = [ '); fprintf(1,'%e ',Xmu); fprintf(']\n');
                fprintf(1,'Qz = [ '); fprintf(1,'%d ',Qz); fprintf(']\n');
                fprintf(1,'Sz = [ '); fprintf(1,'%d ',Sz); fprintf(']\n');
                fprintf(1,'Zmu = [ '); fprintf(1,'%e ',Zmu); fprintf(']\n');
                fprintf(1,'L = [ '); fprintf(1,'%e ',L); fprintf(']\n');
                fprintf(1,'U = [ '); fprintf(1,'%e ',U); fprintf(']\n');
                fprintf(1,'g = [ '); fprintf(1,'%e ',g); fprintf(']\n');
                b = computeBias(X,Z,w,Xmu,Zmu,eta,1);
                acc = (sum(sign(X*w + b) == 1) + sum(sign(Z*w + b) == -1)) / (Xn + Zn) * 100;
                fprintf(1,'f(w)=%e, acc=%f\n',fw,acc); 
            end
            
            % Compute (sub)gradient of norm function at the present w (gives a support hyperplane)
            n = normSubgradient(w,p);
            if verbose
                fprintf(1,'n = [ '); fprintf(1,'%f ',n); fprintf(']\n');
            end
            
            % Compute minimum norm subgradient of the Lagrangian
            [mns Sx Sz Qx Qz Xmu Zmu] = MNS_MDM(X,Z,eta,n,w,Sx,Sz,Qx,Qz,Xmu,Zmu,g,verbose);
            if verbose
                fprintf(1,'g* = [ '); fprintf(1,'%e ',mns); fprintf(']\n');
                fprintf(1,'Qx = [ '); fprintf(1,'%d ',Qx); fprintf(']\n');
                fprintf(1,'Sx = [ '); fprintf(1,'%d ',Sx); fprintf(']\n');
                fprintf(1,'Xmu = [ '); fprintf(1,'%e ',Xmu); fprintf(']\n');
                fprintf(1,'Qz = [ '); fprintf(1,'%d ',Qz); fprintf(']\n');
                fprintf(1,'Sz = [ '); fprintf(1,'%d ',Sz); fprintf(']\n');
                fprintf(1,'Zmu = [ '); fprintf(1,'%e ',Zmu); fprintf(']\n');
            end
            
            % The updating direction is the negative of the MNS
            d = -mns;
            
            if verbose
                fprintf(1,'d = [ '); fprintf(1,'%e ',d); fprintf(']\n');
            end
            
            % Stopping criterion: infinity norm of direction (minimum norm subgradient)
            stop = norm(d,inf);
            % If direction is near to zero, the optimum has been found: stop
            if stop < STOP, break; end;
            
            % Compute stepsize
            [delta,Sx,Sz,Qx,Qz,Xmu,Zmu] = maxSmoothStep(X,Z,eta,w,d,Sx,Sz,Qx,Qz,Xmu,Zmu);
            if verbose
                fprintf(1,'delta=%e\n',delta);
            end
            
            % Perform update
            wPrev = w;
            v = w + delta * d;
            
            % Radial projection back to feasible space
            w = v / norm(v,p);
            
            iters = iters+1;
            
            if verbose
                fprintf(1,'=== End of iteration %d ===\n',iters);
            end
            
            %%% Plot evolution
            if verbose & dim == 2
                b = computeBias(X,Z,w,Xmu,Zmu,eta,1);
                acc = (sum(sign(X*w + b) == 1) + sum(sign(Z*w + b) == -1)) / (Xn + Zn) * 100;
                drawEvol();
            end
        end
        if verbose, fprintf('\n'); end;
    end
    
    % If the intersecting case was encountered, compute now the extreme points and bias
    if intrsct
        % Recover L and U extreme points through the gradient weights
        [L,U] = getLU(X,Z,etaReq,Sx,Sz,Qx,Qz,Xmu,Zmu);
        % Compute classification bias
        b = computeBias(X,Z,w,Xmu,Zmu,eta,1);
    % If not encountered, find the exact lp RCH solution
    else
        % If we have only encountered the trivial (barycentre) case, we don't need to do anything else
        if nontrivial
            [w,b,L,U,lamX,lamZ] = RCHp_FISTA(X,Z,etaReq,p,lamX,lamZ);
        end
    end
    
    % Draw final result
    if verbose & dim == 2
        acc = (sum(sign(X*w + b) == 1) + sum(sign(Z*w + b) == -1)) / (Xn + Zn) * 100;
        drawEvol();
    end;
    
    %%
    %% Auxiliary function to plot evolution of the algorithm when in verbose mode and 2D data
    %%
    function drawEvol()
        %%% SUBFIGURE1: space of inputs
        subplot(1,2,1);
        
        % Plot border of convex hulls
        plot(X(XCH,1),X(XCH,2),'--r');
        hold on
        plot(Z(ZCH,1),Z(ZCH,2),'--b');
        % Plot original points of hulls
        plot(X(:,1),X(:,2),'r+');
        plot(Z(:,1),Z(:,2),'bo');
        % Plot extreme points
        plot(L(1),L(2),'rs','MarkerEdgeColor','k','MarkerFaceColor','r');
        plot(U(1),U(2),'bs','MarkerEdgeColor','k','MarkerFaceColor','b');
        % Compute bias
        b = computeBias(X,Z,w,Xmu,Zmu,eta,1);
        % Compute graphical position of the hyperplane and margins
        sp1 = linspace(plotLimits(1),plotLimits(2),3);
        sp2 = (-b - w(1).*sp1) ./ w(2); plot(sp1,sp2,'k','LineWidth',5);
        sp2 = (-2*b - w(1).*sp1) ./ w(2); plot(sp1,sp2,'--r','LineWidth',3); %%TODO: something wrong with these, changes depending on orient.
        sp2 = (- w(1).*sp1) ./ w(2); plot(sp1,sp2,'--b','LineWidth',3);
        % Plot accuracy
        text(plotLimits(1)+0.1,plotLimits(4)-0.1,sprintf('accuracy = %.1f %%\neta = %f',acc,eta));
        axis(plotLimits);
        title('Inputs space','FontSize',20);
        set(gca,'FontSize',15);
        hold off
        
        %%% SUBFIGURE2: space of w
        subplot(1,2,2);
        
        % Draw objective function colormap
        colormap(jet(256));
        image([C_RANGE(1);C_RANGE(end)],[C_RANGE(1);C_RANGE(end)],bg);
        hold on
        contour(C_RANGE,C_RANGE,plotz,20,'LineWidth',2,'Color','k');
        % Present approximation contour lines
        contour(C_RANGE,C_RANGE,plotz2,20,'--','LineWidth',1,'Color',[0.5 0.5 0.5]);
        % Draw feasible contour
        contour(C_RANGE,C_RANGE,plotn,[1 1],'LineWidth',5,'Color','k');
        contour(C_RANGE,C_RANGE,plotn,[1 1],'LineWidth',2,'Color','b');
        % Draw  w position
        plot(w(1),w(2),'o','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',10);
        quiver(0,0,w(1),w(2),0,'--','LineWidth',3);
        % Texts
        xlabel('w_1'); ylabel('w_2');
        title('w space','FontSize',20);
        set(gca,'YDir','normal','FontSize',15);
        hold off
        
        drawnow;
    end
end

%
% normSubgradient
%
% Computes a subgradient of the norm function, for a given weight vector w and norm p.
% The formula is immediate for p=2 and the general Lp norm.
% For the non-smooth p=1 and p=inf cases, one subgradient is used.
%
% Inputs:
%   - w: current ERCH weight vector.
%   - p: degree of the norm of the ERCH constraint.
%
% Outputs:
%   - n: computed subgradient.
%
function n = normSubgradient(w,p)
    switch p
        case 1
            % Set values near to 0 to exact 0
            idx = find(iszero(w));
            v = w; v(idx) = 0;
            % Sign of the rest of entries
            n = sign(v);
        case 2
            n = 2*w;
        case inf
            % Find values at the maximum
            mx = max(abs(w));
            idx = find(iszero(abs(w)-mx));
            % Sign for those values, 0 for the rest
            n = zeros(size(w));
            n(idx) = sign(w(idx));
        otherwise
            n = p * abs(w).^(p-1) .* sign(p);
    end
end

%
% extremeMargins
%
% Auxiliary function to find the extreme points of the RCH: points with least and most margin
% given the current w.
%
% Inputs:
%   - X: points defining the first convex hull.
%   - Z: points defining the second convex hull.
%   - w: current estimate of the hyperplane w.
%   - eta: reduction factor eta.
%
% Outputs:
%   - L: point with least margin in hull X.
%   - U: point with most margin in hull Z.
%   - Sx: indexes of variable components of the gradient in the hull X.
%   - Sz: indexes of variable components of the gradient in the hull Z.
%   - Qx: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Qz: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Xmu: convex weights of the patterns from hull X.
%   - Zmu: convex weights of the patterns from hull Z.
%
% The computation is done by following the method of Mavroforakis and Theodoridis:
%
%   M. Mavroforakis and S. Theodoridis, “A Geometric Approach to Support
%       Vector Machine (SVM) Classification,” IEEE Transactions on Neural
%       Networks, vol. 17, no. 3, pp. 671–682, May 2006
%
function [L,U,Sx,Sz,Qx,Qz,Xmu,Zmu] = extremeMargins(X,Z,w,eta)
    % Compute number of contributing components for an extreme point that have full (eta) weight
    nFull = floor(1/eta);
    % Limit point: ceiling of 1/eta, but checking we do not get outside the number of available points
    % This constitutes the point immediately after the nFull contributing points in the ordering
    minLim = min(size(X,1),size(Z,1));
    limit = min(ceil(1/eta),minLim);
    % Compute the weight of the limit point.
    % In the special case where eta divides 1 without remainder the limit point is the last full-eta point 
    % for comparison purposes, but has no weight apart from its usual eta weight.
    wlimit = 1-eta*nFull;
    % Set wlimit to 0 if it is too small, to avoid numerical problems
    if iszero(wlimit), wlimit = 0; end

    % Compute margins for every data point
    mX = X*w;  mZ = Z*w;
    
    [mX,iX] = sort(mX,'ascend');
    [mZ,iZ] = sort(mZ,'descend');
        
    % Variable-weight points in the combination
    Sx = iX(find(mX == mX(limit)));
    Sz = iZ(find(mZ == mZ(limit)));
    % Fixed-weight points in the combination
    Qx = iX(find(mX < mX(limit)));
    Qz = iZ(find(mZ > mZ(limit)));
        
    % Obtain explicitly extreme points L and U
    L = eta * sum(X(iX(1:nFull),:),1)' + wlimit * X(iX(limit),:)';
    U = eta * sum(Z(iZ(1:nFull),:),1)' + wlimit * Z(iZ(limit),:)';
    
    % Obtain implicit representation in terms of convex combination weights
    neta = nFull - length(Qx); % numer of S components for which a full eta value will be assigned
    Xmu = zeros(length(Sx),1); % initialize with zeros
    Xmu(1:neta) = eta; % set eta components
    if wlimit > 0, Xmu(neta+1) = wlimit; end; % if any remainder, set to the next point in the combination
    
    neta = nFull - length(Qz); % numer of S components for which a full eta value will be assigned
    Zmu = zeros(length(Sz),1); % initialize with zeros
    Zmu(1:neta) = eta; % set eta components
    if wlimit > 0, Zmu(neta+1) = wlimit; end; % if any remainder, set to the next point in the combination
end

%
% maxSmoothStep
%
% Computes the maximum stepsize in the present smooth region, or until a new non-differentiable
% point is found.
%
% Inputs:
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%   - eta: reduction value for the Reduced Convex Hulls.
%   - w: current classification hyperplane of the ERCH.
%   - d: updating direction for which to find the stepsize.
%   - Sx: indexes of variable components of the gradient in the hull X.
%   - Sz: indexes of variable components of the gradient in the hull Z.
%   - Qx: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Qz: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Xmu: convex weights of the patterns from hull X.
%   - Zmu: convex weights of the patterns from hull Z.
%
% Outputs:
%   - delta: stepsize found.
%   - Sx,Sz,Qx,Qz,Xmu,Zmu: index sets and weights modified accordingly to the expected w value after the update
%
function [delta,Sx,Sz,Qx,Qz,Xmu,Zmu] = maxSmoothStep(X,Z,eta,w,d,Sx,Sz,Qx,Qz,Xmu,Zmu)
    % Default step if it cannot be determined
    DEFSTEP = 100;
    % Tolerance for inexact comparisons
    EPSILON = 1e-7;
    
    % Compute number of contributing components for an extreme point that have full (eta) weight
    nFull = floor(1/eta);
    % Limit point: ceiling of 1/eta, but checking we do not get outside the number of available points
    % This constitutes the point immediately after the nFull contributing points in the ordering
    minLim = min(size(X,1),size(Z,1));
    limit = min(ceil(1/eta),minLim);
    % Compute the weight of the limit point.
    % In the special case where eta divides 1 without remainder the limit point is the last full-eta point 
    % for comparison purposes, but has no weight apart from its usual eta weight.
    wlimit = 1-eta*nFull;
    % Set wlimit to 0 if it is too small, to avoid numerical problems
    if iszero(wlimit), wlimit = 0; end
    
    % Compute margins and inner products with the direction (first order approximation to change in the margin when following d)
    Xm = X*w;  Xd = X*d;
    Zm = Z*w;  Zd = Z*d;
    
    % Compute set of points with no contribution
    Nx = (1:length(X))';   Nx([Sx; Qx]) = [];
    Nz = (1:length(Z))';   Nz([Sz; Qz]) = [];
    
    %% Identify last point with weight for each class.
    % To do so, take the S sets (points with margin = to the last point with weight in the combination),
    % consider how their order will change after an infinitesimal step, and identify the last point with weight there.
    % The change in order after a small step is easily determined by looking at X*d
    
    % Sort S points by change in margin
    [dummy,idx] = sort(Xd(Sx),'ascend');
    % Numer of points in S with full weight
    neta = nFull - length(Qx); if wlimit == 0, neta = neta - 1; end; % Consider also special case where wlimit = 0 (no remainder weight)
    % Because of numerical inaccuracies, sometimes wlimit ~= 0 but there is no pattern in S holding the remaining weight.
    % Thus, we always check whether the index is in range
    id = min(max(neta+1,1),length(idx));
    % Find last point with weight (following the sorting)
    Lx = Sx(idx(id));
    
    % Same for Z hull
    [dummy,idx] = sort(Zd(Sz),'ascend');
    neta = nFull - length(Qz); if wlimit == 0, neta = neta - 1; end;
    id = min(max(neta+1,1),length(idx));
    Lz = Sz(idx(id));

    % Find limiting patterns from X on the stepsize:
    %   - Points in Q whose margin grows faster than that of the limit point
    %   - Points in N whose margin shrinks faster than that of the limit point
    refiqX = find(Xd(Qx) > Xd(Lx)); iqX = Qx(refiqX);
    inX = find(Xd(Nx) < Xd(Lx)); inX = Nx(inX);
    iX = [iqX; inX];
    % This is used to keep track of the set (Q or N) where the constraint comes from
    % 0 = N set, ~= 0 index in Q set
    proc = [refiqX; zeros(size(inX))];
    
    % Find limiting patterns from Z on the stepsize
    refiqZ = find(Zd(Qz) < Zd(Lz)); iqZ = Qz(refiqZ);
    inZ = find(Zd(Nz) > Zd(Lz)); inZ = Nz(inZ);
    iZ = [iqZ; inZ];
    proc = [proc; refiqZ; zeros(size(inZ))];
    
    % Join limitations: the resultant idx contains the indexes of patterns in both X and Z
    % for which a non-differentiability appears for a certain stepsize
    idx = [iX; iZ];
    % c is used to keep track about where each constraint comes from
    c = [ones(size(iX)); zeros(size(iZ))];

    % Compute stepsize: maximum displacement in the current smooth region
    deltas = [(Xm(Lx) - Xm(iX)) ./ (Xd(iX) - Xd(Lx)) ; (Zm(Lz) - Zm(iZ)) ./ (Zd(iZ) - Zd(Lz)) ];
    % Ignora delta = 0 steps (produced by equivalence of margins in the extreme set)
    nonzero = find(deltas>0);
    deltasNonZ = deltas(nonzero);
    % Use default step if it couldn't be determined
    if length(deltasNonZ) == 0 || min(deltasNonZ) == inf
        delta = DEFSTEP; lim = -1;
    else
        delta = min(deltasNonZ);
        % Obtain indexes, classes and procedence sets of limiting patterns (in general only 1, but in some cases several could arise)
        ref = nonzero(iszero(deltasNonZ - delta));
        lims = idx(ref); clims = c(ref); plims = proc(ref);
        
        %% Add limiting patterns to S set
        rmQx = [];  rmQz = [];
        for i=1:length(lims)
            lim = lims(i); % lim is the index of the pattern being inspected
            % Check if in X or Z
            if clims(i) == 1 % X
                % If in Qx, mark to remove from this set and remember its weight
                if plims(i) > 0, rmQx = [rmQx plims(i)]; wpoint = eta;
                else wpoint = 0; end;
                % X case: add to Sx
                Sx = [Sx; lim];  Xmu = [Xmu; wpoint];
            else % Z
                % If in Qz, mark to remove from this set and remember its weight
                if plims(i) > 0, rmQz = [rmQz plims(i)]; wpoint = eta;
                else wpoint = 0; end;
                % Z case: add to Sz
                Sz = [Sz; lim];  Zmu = [Zmu; wpoint];
            end
        end
        % Remove marked elements
        Qx(rmQx) = [];  Qz(rmQz) = [];
    end
end

%
% getLU
% 
% Compute L and U extreme points from subgradient indexes and weights
%
% Inputs:
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%   - eta: reduction value for the Reduced Convex Hulls.
%   - d: updating direction for which to find the stepsize.
%   - Sx: indexes of variable components of the gradient in the hull X.
%   - Sz: indexes of variable components of the gradient in the hull Z.
%   - Qx: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Qz: indexes of fixed (eta) components of the gradient in the hull Z.
%   - Xmu: convex weights of the patterns from hull X.
%   - Zmu: convex weights of the patterns from hull Z.
%
% Outputs:
%   - L: extreme point in the X hull.
%   - U: extreme point in the Z hull.
%
function [L,U] = getLU(X,Z,eta,Sx,Sz,Qx,Qz,Xmu,Zmu)
    L = eta * sum(X(Qx,:),1)' + sum(repmat(Xmu,1,size(X,2)) .* X(Sx,:),1)';
    U = eta * sum(Z(Qz,:),1)' + sum(repmat(Zmu,1,size(Z,2)) .* Z(Sz,:),1)';
end
