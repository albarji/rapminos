%
% computeBias
%
% Computes the bias of the Enu-SVM model.
%
% Inputs
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%   - w: current classification hyperplane of the ERCH.
%   - Xmu: convex weights of the patterns from hull X.
%   - Zmu: convex weights of the patterns from hull Z.
%   - eta: reduction value for the Reduced Convex Hulls.
%   - [alg]: algorithm choice for computing the bias:
%       0: computation using KKT contraints (default)
%       1: minimize empirical error
%
% Outputs:
%   - b: computed SVM bias.
%
% Implementation by Álvaro Barbero (1), Akiko Takeda (2), Jorge López (1).
%  (1) Departamento de Ingeniería Informática and Instituto de Ingeniería del Conocimiento, Universidad Autónoma de Madrid.
%  (2) Department of Mathematical Informatics, The University of Tokyo.
%
% Reference paper:
%   - Geometric Intuition and Algorithms for E\nu-SVM. Álvaro Barbero, Akiko Takeda, Jorge López. Publication pending.
%
function b = computeBias(X,Z,w,Xmu,Zmu,eta,alg)
    % Check arguments
    if ~exist('alg','var'), alg = 0; end

    % Choose algorithm
    if alg == 0
        % Use SVM (KKT) bias
        b = computeSVMBias(X,Z,w,Xmu,Zmu,eta);
    else
        % Use optimal training accuracy bias
        b = optimalTrainingBias(X,Z,w);
    end
end

%
% computeSVMBias
%
% Computes the bias of the model by using the nuSVM KKT conditions,
% instead of the usual geometric interpretation of b standing at the
% bisection between extreme points.
%
% This is done by exploiting the KKT conditions of the ERCH-Margin
% and ERCH-NPP duality, allowing to compute the bias through
% the calculation of the supporting hyperplanes alpha and beta.
%
% Inputs
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%   - w: current classification hyperplane of the ERCH.
%   - Xmu: convex weights of the patterns from hull X.
%   - Zmu: convex weights of the patterns from hull Z.
%   - eta: reduction value for the Reduced Convex Hulls.
%
% Outputs:
%   - b: computed SVM bias.
%
function b = computeSVMBias(X,Z,w,Xmu,Zmu,eta)
    % Find positive class (X) coefficients between 0 and eta
    idx = find(Xmu > 0 & Xmu < eta);
    % Easy case: idx is not empty
    if length(idx) > 0
        % Compute value of alpha = rho - bias through KKT conditions
        alpha = sum( X(idx,:) * w ) / length(idx);
    % Hard case: idx is empty
    else
        % Find positive class (X) coefficients at eta
        idx = find(Xmu == eta);
        % Find max margin
        bottom = max(X(idx,:) * w);
        % Find positive class (X) coefficients at 0
        idx = find(Xmu == 0);
        % If no coefficients at 0, take alpha as bottom
        if length(idx) == 0
            alpha = bottom;
        else
            % Find min margin
            top = min(X(idx,:) * w);
            % r1 is bounded between these bottom and top: take middle point
            alpha = (top + bottom) / 2;
        end
    end
    
    % Find negative class (Z) coefficients between 0 and eta
    idx = find(Zmu > 0 & Zmu < eta);
    % Easy case: idx is not empty
    if length(idx) > 0
        % Compute value of beta = rho + bias through KKT conditions
        beta = sum( Z(idx,:) * w ) / length(idx);
    else
        % Find negative class (Z) coefficients at eta
        idx = find(Zmu == eta);
        % Find min margin
        top = min(Z(idx,:) * w);
        % Find negative class (Z) coefficients at 0
        idx = find(Zmu == 0);
        % If no coefficients at 0, take beta as top
        if length(idx) == 0
            beta = top;
        else
            % Find max margin
            bottom = max(Z(idx,:) * w);
            % beta is bounded between these bottom and top: take middle point
            beta = (top + bottom) / 2;
        end
    end
    
    % Compute bias as b = -(alpha + beta) / 2
    b = -(alpha + beta) / 2;
end

%
% optimalTrainingBias
%
% Computes the bias value producing the smallest error in the training set.
%
%   - X: matrix with points of the first hull (in row form).
%   - Z: matrix with points of the second hull (in row form).
%   - w: current classification hyperplane of the ERCH.
%
% Outputs:
%   - b: computed optimal bias for training data.
function b = optimalTrainingBias(X,Z,w)
    % Compute margin values for all training patterns
    m = [X;Z] * w;
    % Class labels vector
    y = [ones(size(X,1),1); -ones(size(Z,1),1)];
    
    % Sort margins increasingly
    [m,idx] = sort(m);
    % Arrange class labels with the same ordering
    y = y(idx);
    
    % Compute negative class hits as the bias moves over different margin values
    Zhits = cumsum(y == -1);
    % Compute positive class hits as the bias moves over different margin values
    Xhits = bcumsum(y == 1);
    % Sum both hits vectors to get total hits
    hits = Zhits + Xhits;
    % Find best choice
    [best,idx] = max(hits);
    % Get margin value for this -> -bias
    b = -m(idx);
end

%
% bcumsum
%
% Same behaviour as Matlab's cumsum function for 1 dimensional vectors, though
% summing elements in reversed form
%
% Inputs:
%   - x: vector for which to compute accumulative sum, in reversed order.
%
% Outputs:
%   - cs: vector with cumulative sum, in reversed order.
%
function cs = bcumsum(x)
    cs = cumsum(x(end:-1:1));
    cs = cs(end:-1:1);
end
